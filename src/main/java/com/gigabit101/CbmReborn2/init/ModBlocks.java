package com.gigabit101.CbmReborn2.init;

import com.gigabit101.CbmReborn2.blocks.BlockCbm;
import com.gigabit101.CbmReborn2.blocks.BlockCbmBase;
import com.gigabit101.CbmReborn2.itemblocks.ItemBlockCbm;
import com.gigabit101.CbmReborn2.lib.ModInfo;
import com.gigabit101.CbmReborn2.util.LogHelper;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModBlocks {
	
	public static Block CbmBlock;
	
	public static void init()
	{
		
		CbmBlock = new BlockCbm(Material.grass);
		GameRegistry.registerBlock(CbmBlock, 
				ItemBlockCbm.class,
				CbmBlock.getUnlocalizedName().substring(5));
		
		LogHelper.info(ModInfo.MOD_ID + "Blocks Loaded");
	}
	
	
	public static void registerRenders()
	{
		registerRender(CbmBlock);
	}
	
	public static void registerRender(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(ModInfo.MOD_ID.toLowerCase() + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}

}
