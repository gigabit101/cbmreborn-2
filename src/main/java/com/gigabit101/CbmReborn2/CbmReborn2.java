package com.gigabit101.CbmReborn2;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import com.gigabit101.CbmReborn2.init.ModBlocks;
import com.gigabit101.CbmReborn2.lib.ModInfo;
import com.gigabit101.CbmReborn2.proxys.CommonProxy;

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, version = ModInfo.MOD_VERSION)
public class CbmReborn2
{
	@SidedProxy(clientSide = ModInfo.CLIENT_PROXY, serverSide = ModInfo.COMMON_PROXY)
	public static CommonProxy proxy;
	
	public static CreativeTabs tabsCbm = new CreativeTabCbm(
			CreativeTabs.getNextID(), ModInfo.MOD_NAME);
	
	@Mod.EventHandler
	public static void preinit(FMLPreInitializationEvent event)
	{
		
	}

	@Mod.EventHandler
	public static void init(FMLInitializationEvent event)
	{
		ModBlocks.init();
		proxy.registerRender();
	}
	
	@Mod.EventHandler
	public static void postinit(FMLPostInitializationEvent event)
	{
		
	}
}
