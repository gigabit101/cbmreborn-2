package com.gigabit101.CbmReborn2.itemblocks;

import com.gigabit101.CbmReborn2.CbmReborn2;
import com.gigabit101.CbmReborn2.blocks.BlockCbm;
import com.gigabit101.CbmReborn2.blocks.BlockCbmBase;
import com.gigabit101.CbmReborn2.init.ModBlocks;
import com.gigabit101.CbmReborn2.lib.ModBlockNames;
import com.google.common.base.Function;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;

public class ItemBlockCbm extends ItemBlock{

	public ItemBlockCbm(Block block)
	{
		super(block);
	    this.setMaxDamage(0);
	    this.setHasSubtypes(true);	
	}
	
    @Override
    public int getMetadata(int metadata)
    {
        return metadata;
    }
    
    @Override
    public String getUnlocalizedName(ItemStack stack)
    {
        BlockCbmBase block = (BlockCbmBase) this.block;

        if (block.hasPresetProperties())
        {
            return super.getUnlocalizedName() + "." + block.getStateName(block.getStateFromMeta(stack.getMetadata()), false);
        }

        else
            return super.getUnlocalizedName();
    }

}
