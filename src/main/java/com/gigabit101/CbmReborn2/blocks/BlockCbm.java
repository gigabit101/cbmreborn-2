package com.gigabit101.CbmReborn2.blocks;

import java.util.List;

import com.gigabit101.CbmReborn2.CbmReborn2;
import com.gigabit101.CbmReborn2.lib.ModBlockNames;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCbm extends BlockCbmBase{

    public static final PropertyEnum VARIANT_PROP = PropertyEnum.create("variant", CbmType.class);

	public BlockCbm(Material material) 
	{
		super(material);
		this.setUnlocalizedName("cbmBlock");
        this.setDefaultState(this.blockState.getBaseState().withProperty(VARIANT_PROP, CbmType.COBBLE_1));
	}
	
    @Override
    public int damageDropped(IBlockState state)
    {
        return this.getMetaFromState(this.getDefaultState().withProperty(VARIANT_PROP, state.getValue(VARIANT_PROP)));
    }
    
    @Override
    public IBlockState getStateFromMeta(int meta)
    {
        int axis = meta;
        int type = (meta);

        return this.getDefaultState().withProperty(VARIANT_PROP, CbmType.values()[type]);
    }
    
    @Override
    public int getMetaFromState(IBlockState state)
    {
        int baseMeta = ((CbmType) state.getValue(VARIANT_PROP)).ordinal();

        return baseMeta;
    }
    
    @Override
    protected BlockState createBlockState()
    {
        return new BlockState(this, new IProperty[] { VARIANT_PROP });
    }
    
    @Override
    public String getStateName(IBlockState state, boolean fullName)
    {
        return ((CbmType) state.getValue(VARIANT_PROP)).getName() + (fullName ? "_log" : "");
    }
  
	
    public static enum CbmType implements IStringSerializable
    {
        COBBLE_1, COBBLE_2, COBBLE_3, COBBLE_4,
        DIRT_1, DIRT_2, DIRT_3, DIRT_4,
        SAND_1, SAND_2, SAND_3, SAND_4,
        GRAVEL_1, GRAVEL_2, GRAVEL_3, GRAVEL_4,
        GRASS_1, GRASS_2, GRASS_3, GRASS_4,
        SANDSTONE_1, SANDSTONE_2, SANDSTONE_3, SANDSTONE_4;

        @Override
        public String getName()
        {
            return this.name().toLowerCase();
        }

        @Override
        public String toString()
        {
            return getName();
        }
    }  

}
