package com.gigabit101.CbmReborn2.blocks;

import java.util.List;

import com.gigabit101.CbmReborn2.CbmReborn2;
import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockCbmBase extends Block{

    public ImmutableSet<IBlockState> presetStates;

	public BlockCbmBase(Material material) 
	{
		super(material);
		this.setCreativeTab(CbmReborn2.tabsCbm);
	}
	
    public IProperty[] getPresetProperties()
    {
        return null;
    }
    
    public boolean hasPresetProperties()
    {
        return getPresetProperties() != null;
    }
    
    public String getStateName(IBlockState state, boolean fullName)
    {
        String unlocalizedName = state.getBlock().getUnlocalizedName();

        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs tab, List list)
    {
        if (this.hasPresetProperties())
        {
            for (IBlockState state : presetStates)
            {
                list.add(new ItemStack(item, 1, this.getMetaFromState(state)));
            }
        }
        else
        {
            list.add(new ItemStack(item, 1, 0));
        }
    }

}
