package com.gigabit101.CbmReborn2.lib;

public class ModInfo {
	
	public static final String MOD_ID = "cbmreborn2";
	
	public static final String MOD_NAME = "CbmReborn2";
	
	public static final String MOD_VERSION = "1.0.0";
	
	public static final String CLIENT_PROXY = "com.gigabit101.CbmReborn2.proxys.ClientProxy";

	public static final String COMMON_PROXY = "com.gigabit101.CbmReborn2.proxys.CommonProxy";

}
