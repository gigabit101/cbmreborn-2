package com.gigabit101.CbmReborn2;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class CreativeTabCbm extends CreativeTabs{

	public CreativeTabCbm(int index, String label) {
		super(index, label);
	}

	@Override
	public Item getTabIconItem() {
		return Items.diamond;
	}

}
